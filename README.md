# เตรียมตัวก่อนเริ่มเขียน web3.0 (อัพเดท 2022)
## 1 การเลือก Blockchain Network

ควรเริ่มศึกษาโปรโตคอลแต่ล่ะอัน เช่น โปรโตคอลบางตัวมีค่าธรรมเนียมสูงจะส่งผลต่อผู้ใช้ Dapp หรือ บางตัวมีคุณสมบัติพิเศษเช่น การดึงข้อมูลการเข้ารหัส (Alogorand) 

บางคนก็ใช้ภาษาเขียนโปรแกรมที่แตงต่าง เช่น Solana ใช้ Rust

โปรโตคอลที่นิยมมากที่สุดคือ Ethereum และ ภาษาโปรแกรมที่ใช้คือ Solidity

---
## 2 ทำเข้าใจกับ โครงสร้าง Dapp

Solidity ภาษาที่ใช้สำหรับทำ Local Ethereum Env

    Node.js
    Solidity
    JavaScript
    TypeScript
    Python
    React

---
## 3 รู้จักกับ tools และ libraries

### สิ่งที่จะต้องมี

- Metamask 
```
Metamask คือ การเป๋าเงิน Web3 เป็น Extension สำหรับ Browser ใช้สำหรับตรวจสอบสิทธิ์การแลกเปลี่ยนบนบล็อคผ่านการอนุมัติ เป็นมาตราฐาน web3 ไปแล้ว ตัวหลักๆที่นิยมใช้ในฝั่ง Client คือ Ethers.js กับ Web3.js ที่เป็นที่นิยมในเวลานี้
```

- Native IDE
```
แนะนำ Remix เป็นตัวที่ช่วย compile ตรวจสอบตัวแปร ฟังก์ชัน และทำการทดสอบ ได้ด้วย UI แต่มีข้อเสีย Debug ยาก
```
- Development Nodes
```
Hardhat ใช้ทดสอบ compile และ Debug 
```
```
Truffle Suite ประกอบด้วย

- Truffle เป็น Ethereum Virtual Machine (EVM) ใช้สำหรับพัฒนาบน evn ทดสอบ และ เข้าถึง pipeline ของ แต่ล่ะ blockchains ผ่านทาง EVM 

- Ganache ช่วยในการ พัฒนา, deploy และ test contracts ใช้ได้ทั้งที่เป็น application desktop และ command line (RPC)

- Drizzle เป็น frontend libraries ช่วยทำ frontend dapp ตัวหลักๆที่ใช้คือ Redux
```
- Frameworks เรียงตามความน่าสนใจ
```
OpenZeppelin SDK เป็น Toolkit ที่ช่วยในการเขียน smart contract ปัจจุบันมี function มากมายให้เลือกใช้ 

แนะนำการใช้งานจะเป็นลักษณะของ Overriding เป็นส่วนใหญ่
```
```
Moralis เป็น platform ที่ช่วยให้เราไม่จำเป็นต้องสร้างโหนด RPC หรือเรียนรู้วิธีการสร้างกระเป๋าเงิน เพราะมันสร้างมาให้หมดแล้ว
```
```
Embark เป็น platform ช่วยในการ build และ compile dapp
```
```
Brownie เป็น environment สำหรับพัฒนาบน Python 
```
```
Alchemy เป็น platform ทำเป็น api blockchain
```
```
The Graph (GRT) เป็น serach engine สำหรับหาข้อมูล blockchain
```
---
## 4 Develop Dapp

- frontend ส่วนใหญ่จะเป็น JavaScript
- backend ส่วนใหญ่จะเป็น logic code ที่เชื่อมต่อกับ smart contract

## วิธีการรันผ่าน local remixd [ดูวิธีติดตั้ง](https://github.com/ethereum/remixd)
```
remixd -s <path> --remix-ide https://remix.ethereum.org
```

ตัวอย่าง
```
remixd -s ./Solidity --remix-ide https://remix.ethereum.org
```
## Structure of a Contract
1 ระบุ License ที่เราต้องการ [ดูที่นี้](https://spdx.org/licenses/)
```
// SPDX-License-Identifier: <Identifier>
```
ตัวอย่าง
```
// SPDX-License-Identifier: MIT
```
```
// SPDX-License-Identifier: GPL-3.0
```

2 ระบุ Version solidity ที่ต้องการรัน
```
pragma solidity <version>;
```
ตัวอย่าง
```
pragma solidity ^0.8.0;
```
```
pragma solidity >=0.7.0 <0.9.0;
```

3 สร้าง Contracts 
```
contract <name> {
    ...
}
```
ตัวอย่าง
```
contract Bank {
    ...
}
```

4 Variable
```
<type> [private|public] <name>;
```
ตัวอย่าง
```
uint private _balance;
mapping(address => uint) _balances;
```

5 Address
```
mapping(<type index> => <type value>) _balances;
```
ตัวอย่าง
```
mapping(address => uint) _balances;
```

6 Struct
```
struct <name> {
    <type> <variable name>;
}
```
ตัวอย่าง
```
struct Issue {
    bool open; // default is false
    mapping(address => bool) voted;
    uint[] scores;
}
```

7 Function
- แบบไม่คิดค่า Gas
```
function getBalance() public view returns(uint balance) {
    return _balances;
}
```
- แบบคิดค่า Gas
```
function deposite(uint amount) public payable {
    _balance += amount;
}
```

8 Object msg สามารถเรียกใช้เมื่อมีการเรียก จะส่งข้อมูลจาก Metamark มาให้ตาม form (DOM)
- sender
```
msg.sender
```
- value
```
msg.value
```

9 Event
- ประกาศ event
```
event Deposite(uint amount);
```
- Usage
```
function deposite(uint amount) public payable {
    ...
    emit Deposite(amount);
}
```

10 require
```
require(<condition true is pass>, "<ข้อความแสดง เมื่อไม่ตรง condition>");
```
ตัวอย่าง
```
require(amount > 0 && amount <= _balances[msg.sender], "not enough money");
```

11 Custom Modifier 
```
modifier <name> {
    ...
    _; // Continue
}
```
ตัวอย่าง
```
modifier onlyAdmin {
    require(msg.sender == _admin, "Unauthorized");
    _;
}
```
## Cases
- Payable Money
```
payable(<sender>).transfer(<amount>);
```
ตัวอย่าง
```
function withdraw(uint amount) public {
    require(amount > 0 && amount <= _balances[msg.sender], "not enough money");

    payable(msg.sender).transfer(amount);
    _balances[msg.sender] -= amount;
    emit Withdraw(msg.sender, amount);
}
```
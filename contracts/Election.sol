// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

struct Issue {
    bool open; // default is false
    mapping(address => bool) voted;
    mapping(address => uint) ballots;
    uint[] scores;
}

contract Election {
    address _admin;
    mapping(uint => Issue) _issues;
    uint _issueId;
    uint _min;
    uint _max;

    event StatusChange(uint indexed issuedId, bool open);
    event Vote(uint indexed issuedId, address voter, uint indexed select);

    constructor(uint min, uint max) {
        _admin = msg.sender;
        _min = min;
        _max = max;
    }

    modifier onlyAdmin { // Create Modifier custom
        require(msg.sender == _admin, "Unauthorized");
        _; // Continue
    }

    function open() public onlyAdmin {
        require(!_issues[_issueId].open, "Election opening");

        _issueId++;
        _issues[_issueId].open = true;
        _issues[_issueId].scores = new uint[](_max+1);
        emit StatusChange(_issueId, true);
    }

    function close() public onlyAdmin {
        require(_issues[_issueId].open, "Election closed");

        _issues[_issueId].open = false;
        emit StatusChange(_issueId, false);
    }

    function vote(uint select) public {
        require(_issues[_issueId].open, "Election closed");
        require(!_issues[_issueId].voted[msg.sender], "You are voted");
        require(select >= _min && select <= _max, "Incorrect");

        _issues[_issueId].scores[select]++;
        _issues[_issueId].voted[msg.sender] = true;
        _issues[_issueId].ballots[msg.sender] = select;
        emit Vote(_issueId, msg.sender, select);
    }

    function status() public view returns(bool) {
        return _issues[_issueId].open;
    }

    function ballot() public view returns(uint select) {
        require(_issues[_issueId].voted[msg.sender], "You are not vote");

        return _issues[_issueId].ballots[msg.sender];
    }
}
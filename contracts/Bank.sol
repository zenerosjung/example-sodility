// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract Bank {
    uint private _balance;

    mapping(address => uint) _balances;

    event Deposite(address indexed owner, uint amount);
    event Withdraw(address indexed owner, uint amount);

    function deposite() public payable {
        require(msg.value > 0, "deposite money is zero");

        _balances[msg.sender] += msg.value;
        emit Deposite(msg.sender, msg.value);
    }

    function withdraw(uint amount) public {
        require(amount > 0 && amount <= _balances[msg.sender], "not enough money");

        payable(msg.sender).transfer(amount);
        _balances[msg.sender] -= amount;
        emit Withdraw(msg.sender, amount);
    }

    function getBalance() public view returns(uint balance) {
        return _balances[msg.sender];
    }

    function getBalanceOf(address owner) public view returns(uint balance) {
        return _balances[owner];
    }

    // function deposite(uint amount) public {
    //     _balance += amount;
    // }

    // function withdraw(uint amount) public {
    //     require(amount <= _balance, "balance is not enough");
    //     _balance -= amount;
    // }

    // function getBalance() public view returns(uint balance) {
    //     return _balance;
    // }
}